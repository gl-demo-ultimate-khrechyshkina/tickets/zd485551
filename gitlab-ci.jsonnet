local job(thing) =
  {
    image: "alpine:latest",
    stage: "deploy",
    script: "echo I LIKE " + thing
  };

local things = ['blue', 'red', 'green'];
{
  ['job/' + x]: job(x)
  for x in things
}
