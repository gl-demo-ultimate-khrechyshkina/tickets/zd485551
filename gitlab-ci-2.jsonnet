local job(thing) =
  {
    image: "alpine:latest",
    stage: "deploy",
    script: "echo I LIKE " + thing
  };

local things = ['foxes', 'cats', 'goats'];
{
  ['job/' + x]: job(x)
  for x in things
}